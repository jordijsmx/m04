let player = ''; 
let computer = '';

let playerpoints = 0
let computerpoints = 0

function boto(opcio) {

    document.getElementById("player").setAttribute("src", "images/" + opcio + ".png");
    player = opcio;

};

function play() {
    
    let llista = ['rock','paper','scissors','lizard','spock'];
   
    for (let i = 0; i < llista.length; i++) {

        let imatge = llista[i];

        setTimeout(function(imatge) {
            return function() {
                document.getElementById("computer").setAttribute("src", "images/" + imatge + ".png");
            };

        }(imatge), i * 77);
    }

    setTimeout(function() {

        var random = Math.floor(Math.random() * llista.length);
        var seleccio = llista[random];
        document.getElementById("computer").setAttribute("src", "images/" + seleccio + ".png");
        computer = seleccio;
        comparacio();

    }, llista.length * 77);
 
};

function comparacio(){

    if (player == computer) {
        
        document.getElementById("result").innerText = "Draw!";
    
    } else if (
        
        (player == "rock" && (computer == "scissors" || computer == "lizard")) || 
        (player == "paper" && (computer == "rock" || computer == "spock")) ||
        (player == "scissors" && (computer == "paper" || computer == "lizard")) ||
        (player == "lizard" && (computer == "spock" || computer == "paper")) ||
        (player == "spock" && (computer == "rock" || computer == "scissors"))
    ) {
        
        document.getElementById("result").innerText = "Player Wins!";
        playerpoints++;
    
    } else {
    
        document.getElementById("result").innerText = "Computer Wins!";
        computerpoints++;
    
    }
   
    document.getElementById("playerpoints").innerText = "Points: " + playerpoints;
    document.getElementById("computerpoints").innerText = "Points: " + computerpoints;
};