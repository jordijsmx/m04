let $doc := doc('institut.xml')
for $i in $doc/institut/alumnes/alumne
order by $i/nom descending
return <result>{$i/*}</result>