let $doc := doc('institut.xml')
return
<school>
{
  for $i in $doc/institut/alumnes/alumne
  order by $i/nom descending
  return <alu>{$i/*}</alu>
}
</school>