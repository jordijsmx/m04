for $i in doc('institut.xml')/institut/alumnes/alumne
where $i/edat>20
order by $i/nom
return <result>{ (data($i/nom)), (data($i/cognoms)), (data($i/edat)) }</result>