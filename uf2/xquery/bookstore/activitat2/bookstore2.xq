let $books := /bookstore/book[author/lastname = 'Stevens']
return 
  <html>
    <head>
      <title>Book list</title>
      <link rel="stylesheet" href="style.css" />
    </head>
    <body>
      <h1>Book list</h1>
      <table>
        <tr>
          <th>Title</th>
          <th>Year</th>
          <th>Authors</th>
          <th>Editorial</th>
          <th>Price</th>
        </tr>
        {
          for $i in $books
          return
            <tr>
              <td>{data($i/title)}</td>
              <td>{data($i/@year)}</td>
              <td>
                {
                  for $author in $i/author
                  return             
                    if (position() eq last()) then 
                    (data($author/name),' ',data($author/lastname),<br/>)
                }
              </td>
              <td>{data($i/editorial)}</td>
              <td>{data($i/price)}</td>
            </tr>
        }
        <tr>
         <td colspan="3"></td>
         <td><b>Total price:</b></td>
         <td><b>
           {
             sum($books/price)
           }
         </b></td>
        </tr>
      </table>
    </body>
  </html>