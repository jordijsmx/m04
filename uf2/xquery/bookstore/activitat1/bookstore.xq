
  <html>
    <head>
      <title>Book list</title>
      <link rel="stylesheet" href="style.css" />
    </head>
    <body>
      <h1>Book list</h1>
      <table>
        <tr>
          <th>Title</th>
          <th>Editorial</th>
          <th>Price</th>
        </tr>
        {
          for $i in /bookstore/book
          return
            <tr>
              <td>{data($i/title)}</td>
              <td>{data($i/editorial)}</td>
              <td>{data($i/price)}</td>
            </tr>
        }
      </table>
    </body>
  </html>