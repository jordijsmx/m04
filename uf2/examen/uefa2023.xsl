<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
        <html>
            <head>
            <meta charset="UTF-8"/>
            <link rel="stylesheet" href="style.css" />
            </head>
            <body>
                <a  href="https://www.uefa.com/uefachampionsleague/history/seasons/2023/">
                    <img class="uefa" src="logo_UEFA.png"/>
                </a>
                <h1>UEFA Champions League</h1>
                <table>
                    <tr>
                        <th colspan="7"><b>Quarter finals</b></th>
                        <th><b>Winner</b></th>
                    </tr>
                    <xsl:for-each select="//match">
                        <tr>
                            <td>
                                <xsl:variable name="benfica" select="first-leg/local/logo"/>
                                    <img  src="{$benfica}"/>
                            </td>
                            <td><xsl:value-of select="first-leg/local/name"/></td>
                            <td><xsl:value-of select="first-leg/local/goals"/></td>
                            <td> <xsl:text>-</xsl:text></td>
                            <td><xsl:value-of select="first-leg/visitant/goals"/></td>
                            <td><xsl:value-of select="first-leg/visitant/name"/></td>
                            <td>
                                <xsl:variable name="benfica" select="first-leg/visitant/logo"/>
                                <img  src="{$benfica}"/>
                            </td>
                            <td rowspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                <xsl:variable name="benfica" select="second-leg/local/logo"/>
                                    <img  src="{$benfica}"/>
                            </td>
                            <td><xsl:value-of select="second-leg/local/name"/></td>
                            <td><xsl:value-of select="second-leg/local/goals"/></td>
                            <td> <xsl:text>-</xsl:text></td>
                            <td><xsl:value-of select="second-leg/visitant/goals"/></td>
                            <td><xsl:value-of select="second-leg/visitant/name"/></td>
                            <td>
                                <xsl:variable name="benfica" select="second-leg/visitant/logo"/>
                                <img  src="{$benfica}"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
                <p>Total goals: <xsl:value-of select="sum(//match/first-leg/local/goals) + sum(//match/first-leg/visitant/goals) + sum(//match/second-leg/local/goals) + sum(//match/second-leg/visitant/goals)"/></p>
                <p>Number of matches: <xsl:value-of select="count(//match) + count(//match)"/></p>
                <p>Average goals per match: <xsl:value-of select="(sum(//match/first-leg/local/goals) + sum(//match/first-leg/visitant/goals) + sum(//match/second-leg/local/goals) + sum(//match/second-leg/visitant/goals)) div (count(//match) + count(//match))"/></p>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>