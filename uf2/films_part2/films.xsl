<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
    <html>
    <head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="style.css" />
    </head>
            <body>
            <h1>Film List</h1>
                <table>
                    <tr>
                        <th>Title</th>
                        <th>Language</th>
                        <th>Year</th>
                        <th>Country</th>
                        <th>Genre</th>
                        <th>Actors</th>
                        <th>Summary</th>
                    </tr>
                    <xsl:for-each select="//film">
                        <tr>
                            <td><b><xsl:value-of select="title" /></b></td>
                            <td><xsl:value-of select="title/@lang" /></td>
                            <td><xsl:value-of select="year" /></td>
                            <td><xsl:value-of select="country" /></td>
                            <td><xsl:value-of select="genre" /></td>
                            <td>
                                <xsl:for-each select="actor">
                                            <xsl:value-of select="concat(first_name, ' ', last_name)" />
                                            <xsl:if test="position() != last()">, </xsl:if>
                                </xsl:for-each>
                            </td>
                            <td><xsl:value-of select="summary" /></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
    </html>
    </xsl:template>
</xsl:stylesheet>