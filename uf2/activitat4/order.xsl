﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
        <html>
            <head>
            <meta charset="UTF-8"/>
            <link rel="stylesheet" href="style.css" />
            </head>
            
            <table class="table1">
                <tr>
                    <th colspan="2">CUSTOMER DETAILS</th>
                </tr>
                <xsl:for-each select="order/destination">
                    <tr>
                        <td class="td1">Name</td>
                        <td class="td2name"><xsl:value-of select="name" /></td>
                    </tr>
                    <tr>
                        <td class="td1">Address</td>
                        <td class="td2"><xsl:value-of select="address" /></td>
                    </tr>
                    <tr>
                        <td class="td1">City</td>
                        <td class="td2"><xsl:value-of select="city" /></td>
                    </tr>
                    <tr>
                        <td class="td1">P.C.</td>
                        <td class="td2"><xsl:value-of select="postalcode" /></td>
                    </tr>
                </xsl:for-each>
            </table>
            <br/>
            <table class="table2">
                <tr>
                    <th colspan="4">ORDER</th>
                </tr>
                <tr>
                    <td>Product</td>
                    <td>Price</td>
                    <td>Quantity</td>
                    <td>Total</td>
                </tr>
                <xsl:for-each select="order/products/product">
                <xsl:sort select="name"/>
                    <xsl:if test="price > 25 and price <= 100">
                        <tr>
                            <xsl:choose>
                                <xsl:when test="price > 25 and price < 50">
                                    <td bgcolor="yellow"><xsl:value-of select="name"/> (code = <xsl:value-of select="@code"/>)</td>
                                    <td bgcolor="yellow"><xsl:value-of select="price"/></td>
                                    <td bgcolor="yellow"><xsl:value-of select="quantity"/></td>
                                    <td bgcolor="yellow"><xsl:value-of select="price * quantity"/></td>
                                </xsl:when>
                                <xsl:when test="price >= 50 and price < 75">
                                    <td bgcolor="green"><xsl:value-of select="name"/> (code = <xsl:value-of select="@code"/>)</td>
                                    <td bgcolor="green"><xsl:value-of select="price"/></td>
                                    <td bgcolor="green"><xsl:value-of select="quantity"/></td>
                                    <td bgcolor="green"><xsl:value-of select="price * quantity"/></td>
                                </xsl:when>
                                <xsl:when test="price >= 75 and price < 100">
                                    <td bgcolor="red"><xsl:value-of select="name"/> (code = <xsl:value-of select="@code"/>)</td>
                                    <td bgcolor="red"><xsl:value-of select="price"/></td>
                                    <td bgcolor="red"><xsl:value-of select="quantity"/></td>
                                    <td bgcolor="red"><xsl:value-of select="price * quantity"/></td>
                                </xsl:when>
                            </xsl:choose>
                        </tr>
                    </xsl:if>
                </xsl:for-each>
            </table>
        </html>
    </xsl:template>
</xsl:stylesheet>