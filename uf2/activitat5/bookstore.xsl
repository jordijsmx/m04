<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
        <html>
            <head>
            <meta charset="UTF-8"/>
            <link rel="stylesheet" href="style.css" />
            </head>
            <h1>BOOKSTORE</h1>
        <xsl:for-each select="bookstore/book">
         <xsl:sort select="@category" />
            <table>
                <tr>
                    <td class="left"><b>Title (lang: <xsl:value-of select="title/@lang"/>)</b></td>
                    <td><xsl:value-of select="title" /></td>
                </tr>
                <tr>
                    <td class="left"><b>Category</b></td>
                    <td><xsl:value-of select="@category" /></td>
                </tr>
                <tr>
                    <td class="left"><b>Year</b></td>
                    <td><xsl:value-of select="year" /></td>
                </tr>
                <tr>
                    <td class="left"><b>Price</b></td>
                    <td>
                        <xsl:value-of select="price" />
                         <xsl:text> €</xsl:text>
                    </td>
                </tr>
                <tr>
                    <td class="left"><b>Format</b></td>
                    <td><xsl:value-of select="format/@type" /></td>
                </tr>
                <tr>
                    <td class="left"><b>ISBN</b></td>
                    <td><xsl:value-of select="isbn" /></td>
                </tr>
                <tr class="authors">
                    <td colspan="2"><b>Authors:</b></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <xsl:for-each select="author">
                            <xsl:sort select="." />
                            <xsl:value-of select="." />
                            <xsl:if test="position() != last()"><br /></xsl:if>
                        </xsl:for-each>
                    </td>
                </tr>
                <br/>
            </table>
        </xsl:for-each>
        </html>
    </xsl:template>
</xsl:stylesheet>