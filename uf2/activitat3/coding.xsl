<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >    
    <html>
    <head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="style.css" />
    </head>
            <body>
                <table>
                    <tr>
                        <th>Logo</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>License</th>
                    </tr>
                    <xsl:for-each select="//program">
                        <tr>
                            <td><xsl:variable name="logoimg" select="logo"/>
                                <img  src="{$logoimg}"/></td>
                            <td><xsl:value-of select="name" /></td>
                            <td><xsl:value-of select="type" /></td>
                            <td><xsl:value-of select="license" /></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
    </html>
    </xsl:template>
</xsl:stylesheet>