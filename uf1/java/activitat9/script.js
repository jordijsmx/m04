function convertTemp() {
    let from = document.getElementById("select1").value;
    let to = document.getElementById("select2").value;
    let input = parseFloat(document.getElementById("input").value);

    let resultat;
    let formula;

    if (from == "C" && to == "F") {
        resultat = input * (9/5) + 32;
        formula = "F = C * (9/5) + 32"

    } else if (from == "C" && to == "K") {
        resultat = input + 273.15;
        formula = "K = C + 273.15"

    } else if (from == "C" && to == "R") {
        resultat = input * (9/5) + 32 + 459.67;
        formula = "R = C * (9/5) + 32 + 459.67"

    } else if (from == "F" && to == "C") {
        resultat = (input - 32) * 5/9;
        formula = "C = (F - 32) * 5/9"

    } else if (from == "F" && to == "K") {
        resultat = (input - 32) * 5/9 + 273.15;
        formula = "K = (F - 32) * 5/9 + 273.15"

    } else if (from == "F" && to == "R") {
        resultat = input + 459.67;
        formula = "R = F + 459.67"

    } else if (from == "K" && to == "C") {
        resultat = input - 273.15;
        formula = "C = K - 273.15"

    } else if (from == "K" && to == "F") {
        resultat = input * (9/5) - 459.67;
        formula = "F = K * (9/5) - 459.67"

    } else if (from == "K" && to == "R") {
        resultat = input * 9/5;
        formula = "R = K * 9/5"

    } else if (from == "R" && to == "C") {
        resultat = (input - 32 - 459.67) * 9/5;
        formula = "C = (R - 32 - 459.67) * 9/5"

    } else if (from == "R" && to == "F") {
        resultat = input - 459.67;
        formula = "F = R - 459.67"

    } else if (from == "R" && to == "K") {
        resultat = input * 5/9;
        formula = "K = R * 5/9"

    }

    document.getElementById("resultat").value = resultat.toFixed(2);
    document.getElementById("formula").innerHTML = formula;

}