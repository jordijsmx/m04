function calcularDNI(dni) {
    let cadena = "TRWAGMYFPDXBNJZSQVHLCKET";
    dni = parseInt(dni);
    let posicio = dni % 23;
    return cadena[posicio];
}

function lletraDNI() {
    let numero = document.getElementById('dniNumber');
    let numeroDNI = parseInt(numero.value)

    if (numeroDNI >= 10000000 && numeroDNI <= 99999999) {
        let lletraDNI = calcularDNI(numeroDNI);
        document.getElementById('result1').innerHTML = 'The letter for DNI ' + numeroDNI + ' is: ' + lletraDNI;
    
    } else {
        document.getElementById('result1').innerHTML = 'Please enter a valid DNI number between 10000000 and 99999999.';
    }
}

function generarDNI() {
    let numero = Math.floor(Math.random() * (99999999 - 10000000 + 1)) + 10000000;
    let lletra = calcularDNI(numero);

    document.getElementById("result2").innerHTML = "The DNI is: " + numero + lletra;
}